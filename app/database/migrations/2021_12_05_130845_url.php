<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Url extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('urls', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('token')->unique()->nullable()->comment('Токен ссылка');
            $table->integer('token_length')->comment('Разрядность ');
            $table->text('origin_url')->unique()->comment('Оригинальная ссылка');
            $table->integer('request_count')->default(0)->comment('Кол-во обращений к ссылке');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('urls');
    }
}
