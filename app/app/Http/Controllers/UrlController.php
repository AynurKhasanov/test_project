<?php

namespace App\Http\Controllers;

use App\Jobs\IncUrlRequest;
use App\Models\Url;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class UrlController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $url = $request->post('url');

        if (!($url)){
            return response()->json(['status' => 'not found'], 404);
        }

        if (Cache::has($url)){
            return response()->json(['status' => 'repeat few second'], 200);
        }

        if ($exist_url = Url::getOriginUrl($url)) {
            /**
             * @var $exist_url Url
             */
            $token = $exist_url->token;
        } else {
            Cache::add($url, true, 10);
            $new_url = new Url();
            $new_url->origin_url = $url;
            $new_url->token_length = strlen($new_url->token);
            $new_url->save();

            $token =  $new_url->makeUniqueTokenUrl();
            Cache::forget($url);
        }

        return response()->json(
            [
                'status' => 'ok',
                'url' => $request->getUriForPath('/' . $token)
            ]
        );
    }

    public function redirect(Request $request): JsonResponse|RedirectResponse
    {
        $token = $request->route('token');

        if ($exist_url = Url::getTokenUrl($token)) {
            /**
             * @var $exist_url Url
             */

            IncUrlRequest::dispatch($exist_url)->onQueue('inc_url_request');

            return redirect()->away($exist_url->origin_url, 301);
        }

        return response()->json(['status' => 'not found'], 404);
    }

    public function state(Request $request): JsonResponse
    {
        $token = $request->get('token');

        if ($exist_url = Url::getTokenUrl($token)) {
            /**
             * @var $exist_url Url
             */

            return response()->json(['request_count' => $exist_url->request_count]);
        }

        return response()->json(['status' => 'not found'], 404);
    }

    public function urlCount(): JsonResponse
    {
        return response()->json([Url::getUrlCounts()]);
    }
}
