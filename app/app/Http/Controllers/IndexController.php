<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(Request $request): View
    {
        $posts = Post::query()->orderByDesc('updated_at');

        if ($request->get('category')) {
            $posts->where('id_category', '=', $request->get('category'));
        }

        $posts = $posts->paginate(5)->withQueryString();
        $categories = Category::query()->get();

        return view('index', [
            'posts'      => $posts,
            'categories' => $categories
        ]);
    }

    public function view(int $id): View
    {
        $post = Post::query()->findOrFail($id);

        return view('view', compact('post'));
    }
}
