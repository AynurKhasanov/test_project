<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Http\Models\Post
 *
 * @property int $id
 * @property string $name
*/

class Post extends Model
{
    use HasFactory;

    protected $with = ['category'];

    protected $fillable = [
        'id_category',
        'name',
        'text'
    ];

    public function category(): HasOne
    {
        return $this->hasOne(Category::class, 'id', 'id_category');
    }
}
