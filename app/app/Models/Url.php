<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

/**
 * Class Url
 * @package App\Models
 *
 * @property string $id
 * @property string $token
 * @property integer $token_length
 * @property string $origin_url
 * @property integer $request_count
 */
class Url extends Model
{
    use HasFactory;

    private const SYMBOLS = '1234567890abcdefghijklmnopqrstuvwxyz';

    private const SYMBOLS_COUNT = 36;

    public function makeUniqueTokenUrl(): string
    {
        $this->token = str_pad(
            base_convert(
                (int)$this->id,
                10, 36
            ),
            3,
            "0",
            STR_PAD_LEFT
        );
        $this->token_length = strlen($this->token);
        $this->save();

        Cache::decrement('token_count_rage_' . $this->token_length);

        return $this->token;
    }

    public static function getTokenUrl(string $token): Model|Builder|null
    {
        return Url::query()->where('token', $token)->first();
    }

    public static function getOriginUrl(string $origin_url): Model|Builder|null
    {
        return Url::query()->where('origin_url', $origin_url)->first();
    }

    public static function getUrlCounts(): array
    {
        if (!Cache::has('token_count_rage_3')
            || !Cache::has('token_count_rage_4')
            || !Cache::has('token_count_rage_5')
            || !Cache::has('token_count_rage_6')) {
            self::initUrlsCounter();
        }

        return [
            3 => Cache::get('token_count_rage_3'),
            4 => Cache::get('token_count_rage_4'),
            5 => Cache::get('token_count_rage_5'),
            6 => Cache::get('token_count_rage_6'),
        ];
    }

    public static function initUrlsCounter(): void
    {
        $total_counts = [
            3 => pow(self::SYMBOLS_COUNT, 3),
            4 => pow(self::SYMBOLS_COUNT, 4),
            5 => pow(self::SYMBOLS_COUNT, 5),
            6 => pow(self::SYMBOLS_COUNT, 6),
        ];

        $urls = Url::query()
            ->selectRaw('token_length, count(id) as total')
            ->groupBy('token_length')
            ->get()
            ->keyBy('token_length')
            ->toArray();

        foreach ($total_counts as $key => $value) {
            $total = $urls[$key]['total'] ?? 0;
            Cache::forever('token_count_rage_' . $key, ($value - $total));
        }
    }
}
