@extends('layouts.app')

@section('content')

    <form class="row g-3" method="GET" action="{{ route('index') }}">
        <div class="col-auto">
            <select aria-label="category" name="category" class="form-control custom-select">
                <option value="">Select category</option>
                @foreach($categories as $category)
                    <option value="{{ $category->id }}" @if($category->id == request()->get('category')) selected @endif>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="col-auto">
            <button type="submit" class="btn btn-primary mb-3">Confirm filter</button>
        </div>
    </form>

    <br>

    @foreach ($posts as $post)

        <div class="card">
            <div class="card-body">
                <h5 class="card-title">{{ $post->name }}
                    <span class="badge bg-secondary">
                        {{ $post->category->name }}
                    </span>
                    <span class="badge bg-secondary">
                        {{ $post->updated_at->format('H:i:s d.m.Y') }}
                    </span>
                </h5>
                <p class="card-text">{{ $post->text }}</p>
                <a href="{{route('view', $post->id)}}" class="btn btn-primary">Detail</a>
            </div>
        </div>

    @endforeach

    {{$posts->links('vendor.pagination.bootstrap-4')}}

@endsection